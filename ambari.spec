%define __jar_repack %{nil}

%global debug_package %{nil}

%global compile_for_local 0

%global with_debug 0

%global with_tests 0

Name: ambari
Version: 2.7.6
Release: 1
Summary: Ambari Main
License: Apache 2.0
Vendor: Apache Software Foundation
URL: https://ambari.apache.org
Source0: https://github.com/apache/ambari/archive/refs/tags/release-2.7.6.tar.gz
Source1: settings.xml
Source2: node-v4.5.0-linux-arm64.tar.gz
Source3: node-v4.5.0-linux-x64.tar.gz
Source4: yarn-v0.23.2.tar.gz
Source5: phantomjs-2.1.1-linux-x86_64.tar.bz2
Source6: phantomjs-1.9.8-linux-x86_64.tar.bz2
Source7: https://repo.hortonworks.com/content/repositories/releases/org/apache/storm/storm-core/0.10.0.2.3.0.0-2557/storm-core-0.10.0.2.3.0.0-2557.jar
Source8: storm-core-0.10.0.2.3.0.0-2557.pom
Source9: storm-0.10.0.2.3.0.0-2557.pom
Source10: https://repo.hortonworks.com/content/repositories/releases/org/apache/hadoop/hadoop-auth/2.7.1.2.3.0.0-2557/hadoop-auth-2.7.1.2.3.0.0-2557.jar
Source11: hadoop-auth-2.7.1.2.3.0.0-2557.pom
Source12: hadoop-project-2.7.1.2.3.0.0-2557.pom
Source13: hadoop-main-2.7.1.2.3.0.0-2557.pom
Source14: https://repo.hortonworks.com/repository/released_old/org/apache/zookeeper/zookeeper/3.4.6.2.3.0.0-2557/zookeeper-3.4.6.2.3.0.0-2557.jar
Source15: zookeeper-3.4.6.2.3.0.0-2557.pom
Source16: node-v8.6.0-linux-x64.tar.gz
Source17: yarn-v1.1.0.tar.gz
Source18: node-v8.6.0-linux-arm64.tar.gz
Source19: webresource.tar.gz

Patch0: 0001-init.patch

Group: Development
Packager: Apache Software Foundation
BuildRequires: java-1.8.0-openjdk-devel
BuildRequires: maven
BuildRequires: python3-devel
Requires: java-1.8.0-openjdk
Requires: postgresql-server >= 8.1
Requires: openssl

autoprov: yes
autoreq: yes

%description
Apache Ambari is a tool for provisioning, managing, and monitoring Apache Hadoop clusters. Ambari consists of a set of RESTful APIs and a browser-based management interface.

%prep
%autosetup -p1 -n ambari-release-%{version}
tar -zxvf %{SOURCE19}

%build
cp %{SOURCE1} ./settings.xml
mkdir -p /home/abuild/.m2/repository/com/github/eirslett/node/4.5.0/
mkdir -p /home/abuild/.m2/repository/com/github/eirslett/node/8.6.0/
mkdir -p /tmp/npm_config_tmp/phantomjs/
mkdir -p /home/abuild/rpmbuild/BUILD/apache-ambari-2.7.6/ambari-infra/ambari-infra-solr-client/target/migrate/
mkdir -p /home/abuild/.m2/repository/com/github/eirslett/yarn/0.23.2/yarn-0.23.2./
mkdir -p /home/abuild/.m2/repository/org/apache/storm/storm-core/0.10.0.2.3.0.0-2557/
mkdir -p /home/abuild/.m2/repository/org/apache/storm/storm/0.10.0.2.3.0.0-2557/
mkdir -p /home/abuild/.m2/repository/org/apache/hadoop/hadoop-auth/2.7.1.2.3.0.0-2557/
mkdir -p /home/abuild/.m2/repository/org/apache/hadoop/hadoop-project/2.7.1.2.3.0.0-2557/
mkdir -p /home/abuild/.m2/repository/org/apache/hadoop/hadoop-main/2.7.1.2.3.0.0-2557/
mkdir -p /home/abuild/.m2/repository/org/apache/zookeeper/zookeeper/3.4.6.2.3.0.0-2557/
mkdir -p /home/abuild/.m2/repository/com/github/eirslett/yarn/1.1.0/
mkdir -p /tmp/logsearch_npm_config_tmp/phantomjs/
cp %{SOURCE2} /home/abuild/.m2/repository/com/github/eirslett/node/4.5.0/node-4.5.0-linux-arm64.tar.gz
cp %{SOURCE3} /home/abuild/.m2/repository/com/github/eirslett/node/4.5.0/node-4.5.0-linux-x64.tar.gz
cp %{SOURCE4} /home/abuild/.m2/repository/com/github/eirslett/yarn/0.23.2/yarn-0.23.2./yarn-v0.23.2.tar.gz
cp %{SOURCE5} /tmp/npm_config_tmp/phantomjs/phantomjs-2.1.1-linux-x86_64.tar.bz2
cp %{SOURCE5} /tmp/logsearch_npm_config_tmp/phantomjs/phantomjs-2.1.1-linux-x86_64.tar.bz2
cp %{SOURCE6} /tmp/npm_config_tmp/phantomjs/phantomjs-1.9.8-linux-x86_64.tar.bz2
cp %{SOURCE7} /home/abuild/.m2/repository/org/apache/storm/storm-core/0.10.0.2.3.0.0-2557/storm-core-0.10.0.2.3.0.0-2557.jar
cp %{SOURCE8} /home/abuild/.m2/repository/org/apache/storm/storm-core/0.10.0.2.3.0.0-2557/storm-core-0.10.0.2.3.0.0-2557.pom
cp %{SOURCE9} /home/abuild/.m2/repository/org/apache/storm/storm/0.10.0.2.3.0.0-2557/storm-0.10.0.2.3.0.0-2557.pom
cp %{SOURCE10} /home/abuild/.m2/repository/org/apache/hadoop/hadoop-auth/2.7.1.2.3.0.0-2557/hadoop-auth-2.7.1.2.3.0.0-2557.jar
cp %{SOURCE11} /home/abuild/.m2/repository/org/apache/hadoop/hadoop-auth/2.7.1.2.3.0.0-2557/hadoop-auth-2.7.1.2.3.0.0-2557.pom
cp %{SOURCE12} /home/abuild/.m2/repository/org/apache/hadoop/hadoop-project/2.7.1.2.3.0.0-2557/hadoop-project-2.7.1.2.3.0.0-2557.pom
cp %{SOURCE13} /home/abuild/.m2/repository/org/apache/hadoop/hadoop-main/2.7.1.2.3.0.0-2557/hadoop-main-2.7.1.2.3.0.0-2557.pom
cp %{SOURCE14} /home/abuild/.m2/repository/org/apache/zookeeper/zookeeper/3.4.6.2.3.0.0-2557/zookeeper-3.4.6.2.3.0.0-2557.pom
cp %{SOURCE15} /home/abuild/.m2/repository/org/apache/zookeeper/zookeeper/3.4.6.2.3.0.0-2557/zookeeper-3.4.6.2.3.0.0-2557.jar
cp %{SOURCE16} /home/abuild/.m2/repository/com/github/eirslett/node/8.6.0/node-8.6.0-linux-x64.tar.gz
cp %{SOURCE17} /home/abuild/.m2/repository/com/github/eirslett/yarn/1.1.0/yarn-1.1.0.tar.gz

%install
mkdir -p %{buildroot}/opt/ambari
sysVer="aarch64"
if [ "$(uname -m)" = ${sysVer} ];
then
	cp %{SOURCE18} /home/abuild/.m2/repository/com/github/eirslett/node/8.6.0/node-8.6.0-linux-arm64.tar.gz
fi
mvn -B clean install package rpm:rpm -s settings.xml -DskipTests -Dmaven.test.skip=true -Drat.skip=true
cp -rf ./ambari-server/target/rpm/ambari-server/RPMS/x86_64 %{buildroot}/opt/ambari/ambari-server
cp -rf ./ambari-agent/target/rpm/ambari-agent/RPMS/x86_64 %{buildroot}/opt/ambari/ambari-agent

%files
/opt

%changelog
* Wed Jul 13 2022 jhu <2416510131@qq.com> - 2.7.6-1
- Init package